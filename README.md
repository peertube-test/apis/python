# Python API client for PeerTube

This Python package is automatically generated from [PeerTube's REST API](https://docs.joinpeertube.org/api-rest-reference.html),
using the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 2.1.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

For more information, please visit [https://joinpeertube.org](https://joinpeertube.org)

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage

```sh
pip install git+https://framagit.org/peertube-test/apis/python.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://framagit.org/peertube-test/apis/python.git`)

Then import the package:
```python
import peertube
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint


# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort column (-createdAt for example) (optional)

    try:
        # Get all accounts
        api_response = api_instance.accounts_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_get: %s\n" % e)
    
```

## Documentation for API Endpoints

All URIs are relative to *https://peertube.cpy.re/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountsApi* | [**accounts_get**](docs/AccountsApi.md#accounts_get) | **GET** /accounts | Get all accounts
*AccountsApi* | [**accounts_name_get**](docs/AccountsApi.md#accounts_name_get) | **GET** /accounts/{name} | Get the account by name
*AccountsApi* | [**accounts_name_videos_get**](docs/AccountsApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account
*ConfigApi* | [**config_about_get**](docs/ConfigApi.md#config_about_get) | **GET** /config/about | Get the instance about page content
*ConfigApi* | [**config_custom_delete**](docs/ConfigApi.md#config_custom_delete) | **DELETE** /config/custom | Delete the runtime configuration of the server
*ConfigApi* | [**config_custom_get**](docs/ConfigApi.md#config_custom_get) | **GET** /config/custom | Get the runtime configuration of the server
*ConfigApi* | [**config_custom_put**](docs/ConfigApi.md#config_custom_put) | **PUT** /config/custom | Set the runtime configuration of the server
*ConfigApi* | [**config_get**](docs/ConfigApi.md#config_get) | **GET** /config | Get the public configuration of the server
*JobApi* | [**jobs_state_get**](docs/JobApi.md#jobs_state_get) | **GET** /jobs/{state} | Get list of jobs
*MyUserApi* | [**users_me_avatar_pick_post**](docs/MyUserApi.md#users_me_avatar_pick_post) | **POST** /users/me/avatar/pick | Update current user avatar
*MyUserApi* | [**users_me_get**](docs/MyUserApi.md#users_me_get) | **GET** /users/me | Get current user information
*MyUserApi* | [**users_me_put**](docs/MyUserApi.md#users_me_put) | **PUT** /users/me | Update current user information
*MyUserApi* | [**users_me_subscriptions_exist_get**](docs/MyUserApi.md#users_me_subscriptions_exist_get) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for the current user
*MyUserApi* | [**users_me_subscriptions_get**](docs/MyUserApi.md#users_me_subscriptions_get) | **GET** /users/me/subscriptions | Get subscriptions of the current user
*MyUserApi* | [**users_me_subscriptions_post**](docs/MyUserApi.md#users_me_subscriptions_post) | **POST** /users/me/subscriptions | Add subscription to the current user
*MyUserApi* | [**users_me_subscriptions_subscription_handle_delete**](docs/MyUserApi.md#users_me_subscriptions_subscription_handle_delete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of the current user for a given uri
*MyUserApi* | [**users_me_subscriptions_subscription_handle_get**](docs/MyUserApi.md#users_me_subscriptions_subscription_handle_get) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of the current user for a given uri
*MyUserApi* | [**users_me_subscriptions_videos_get**](docs/MyUserApi.md#users_me_subscriptions_videos_get) | **GET** /users/me/subscriptions/videos | Get videos of subscriptions of the current user
*MyUserApi* | [**users_me_video_quota_used_get**](docs/MyUserApi.md#users_me_video_quota_used_get) | **GET** /users/me/video-quota-used | Get current user used quota
*MyUserApi* | [**users_me_videos_get**](docs/MyUserApi.md#users_me_videos_get) | **GET** /users/me/videos | Get videos of the current user
*MyUserApi* | [**users_me_videos_imports_get**](docs/MyUserApi.md#users_me_videos_imports_get) | **GET** /users/me/videos/imports | Get video imports of current user
*MyUserApi* | [**users_me_videos_video_id_rating_get**](docs/MyUserApi.md#users_me_videos_video_id_rating_get) | **GET** /users/me/videos/{videoId}/rating | Get rating of video by its id, among those of the current user
*SearchApi* | [**search_videos_get**](docs/SearchApi.md#search_videos_get) | **GET** /search/videos | Get the videos corresponding to a given query
*ServerFollowingApi* | [**server_followers_get**](docs/ServerFollowingApi.md#server_followers_get) | **GET** /server/followers | Get followers of the server
*ServerFollowingApi* | [**server_following_get**](docs/ServerFollowingApi.md#server_following_get) | **GET** /server/following | Get servers followed by the server
*ServerFollowingApi* | [**server_following_host_delete**](docs/ServerFollowingApi.md#server_following_host_delete) | **DELETE** /server/following/{host} | Unfollow a server by hostname
*ServerFollowingApi* | [**server_following_post**](docs/ServerFollowingApi.md#server_following_post) | **POST** /server/following | Follow a server
*UserApi* | [**accounts_name_ratings_get**](docs/UserApi.md#accounts_name_ratings_get) | **GET** /accounts/{name}/ratings | Get ratings of an account by its name
*UserApi* | [**users_get**](docs/UserApi.md#users_get) | **GET** /users | Get a list of users
*UserApi* | [**users_id_delete**](docs/UserApi.md#users_id_delete) | **DELETE** /users/{id} | Delete a user by its id
*UserApi* | [**users_id_get**](docs/UserApi.md#users_id_get) | **GET** /users/{id} | Get user by its id
*UserApi* | [**users_id_put**](docs/UserApi.md#users_id_put) | **PUT** /users/{id} | Update user profile by its id
*UserApi* | [**users_post**](docs/UserApi.md#users_post) | **POST** /users | Creates user
*UserApi* | [**users_register_post**](docs/UserApi.md#users_register_post) | **POST** /users/register | Register a user
*VideoApi* | [**accounts_name_videos_get**](docs/VideoApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account
*VideoApi* | [**video_channels_channel_handle_videos_get**](docs/VideoApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
*VideoApi* | [**videos_categories_get**](docs/VideoApi.md#videos_categories_get) | **GET** /videos/categories | Get list of video categories known by the server
*VideoApi* | [**videos_get**](docs/VideoApi.md#videos_get) | **GET** /videos | Get list of videos
*VideoApi* | [**videos_id_delete**](docs/VideoApi.md#videos_id_delete) | **DELETE** /videos/{id} | Delete a video by its id
*VideoApi* | [**videos_id_description_get**](docs/VideoApi.md#videos_id_description_get) | **GET** /videos/{id}/description | Get a video description by its id
*VideoApi* | [**videos_id_get**](docs/VideoApi.md#videos_id_get) | **GET** /videos/{id} | Get a video by its id
*VideoApi* | [**videos_id_give_ownership_post**](docs/VideoApi.md#videos_id_give_ownership_post) | **POST** /videos/{id}/give-ownership | Request change of ownership for a video you own, by its id
*VideoApi* | [**videos_id_put**](docs/VideoApi.md#videos_id_put) | **PUT** /videos/{id} | Update metadata for a video by its id
*VideoApi* | [**videos_id_views_post**](docs/VideoApi.md#videos_id_views_post) | **POST** /videos/{id}/views | Add a view to the video by its id
*VideoApi* | [**videos_id_watching_put**](docs/VideoApi.md#videos_id_watching_put) | **PUT** /videos/{id}/watching | Set watching progress of a video by its id for a user
*VideoApi* | [**videos_imports_post**](docs/VideoApi.md#videos_imports_post) | **POST** /videos/imports | Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
*VideoApi* | [**videos_languages_get**](docs/VideoApi.md#videos_languages_get) | **GET** /videos/languages | Get list of languages known by the server
*VideoApi* | [**videos_licences_get**](docs/VideoApi.md#videos_licences_get) | **GET** /videos/licences | Get list of video licences known by the server
*VideoApi* | [**videos_ownership_get**](docs/VideoApi.md#videos_ownership_get) | **GET** /videos/ownership | Get list of video ownership changes requests
*VideoApi* | [**videos_ownership_id_accept_post**](docs/VideoApi.md#videos_ownership_id_accept_post) | **POST** /videos/ownership/{id}/accept | Refuse ownership change request for video by its id
*VideoApi* | [**videos_ownership_id_refuse_post**](docs/VideoApi.md#videos_ownership_id_refuse_post) | **POST** /videos/ownership/{id}/refuse | Accept ownership change request for video by its id
*VideoApi* | [**videos_privacies_get**](docs/VideoApi.md#videos_privacies_get) | **GET** /videos/privacies | Get list of privacy policies supported by the server
*VideoApi* | [**videos_upload_post**](docs/VideoApi.md#videos_upload_post) | **POST** /videos/upload | Upload a video file with its metadata
*VideoAbuseApi* | [**videos_abuse_get**](docs/VideoAbuseApi.md#videos_abuse_get) | **GET** /videos/abuse | Get list of reported video abuses
*VideoAbuseApi* | [**videos_id_abuse_post**](docs/VideoAbuseApi.md#videos_id_abuse_post) | **POST** /videos/{id}/abuse | Report an abuse, on a video by its id
*VideoBlacklistApi* | [**videos_blacklist_get**](docs/VideoBlacklistApi.md#videos_blacklist_get) | **GET** /videos/blacklist | Get list of videos on blacklist
*VideoBlacklistApi* | [**videos_id_blacklist_delete**](docs/VideoBlacklistApi.md#videos_id_blacklist_delete) | **DELETE** /videos/{id}/blacklist | Delete an entry of the blacklist of a video by its id
*VideoBlacklistApi* | [**videos_id_blacklist_post**](docs/VideoBlacklistApi.md#videos_id_blacklist_post) | **POST** /videos/{id}/blacklist | Put on blacklist a video by its id
*VideoCaptionApi* | [**videos_id_captions_caption_language_delete**](docs/VideoCaptionApi.md#videos_id_captions_caption_language_delete) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
*VideoCaptionApi* | [**videos_id_captions_caption_language_put**](docs/VideoCaptionApi.md#videos_id_captions_caption_language_put) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
*VideoCaptionApi* | [**videos_id_captions_get**](docs/VideoCaptionApi.md#videos_id_captions_get) | **GET** /videos/{id}/captions | Get list of video&#39;s captions
*VideoChannelApi* | [**accounts_name_video_channels_get**](docs/VideoChannelApi.md#accounts_name_video_channels_get) | **GET** /accounts/{name}/video-channels | Get video channels of an account by its name
*VideoChannelApi* | [**video_channels_channel_handle_delete**](docs/VideoChannelApi.md#video_channels_channel_handle_delete) | **DELETE** /video-channels/{channelHandle} | Delete a video channel by its id
*VideoChannelApi* | [**video_channels_channel_handle_get**](docs/VideoChannelApi.md#video_channels_channel_handle_get) | **GET** /video-channels/{channelHandle} | Get a video channel by its id
*VideoChannelApi* | [**video_channels_channel_handle_put**](docs/VideoChannelApi.md#video_channels_channel_handle_put) | **PUT** /video-channels/{channelHandle} | Update a video channel by its id
*VideoChannelApi* | [**video_channels_channel_handle_videos_get**](docs/VideoChannelApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
*VideoChannelApi* | [**video_channels_get**](docs/VideoChannelApi.md#video_channels_get) | **GET** /video-channels | Get list of video channels
*VideoChannelApi* | [**video_channels_post**](docs/VideoChannelApi.md#video_channels_post) | **POST** /video-channels | Creates a video channel for the current user
*VideoCommentApi* | [**videos_id_comment_threads_get**](docs/VideoCommentApi.md#videos_id_comment_threads_get) | **GET** /videos/{id}/comment-threads | Get the comment threads of a video by its id
*VideoCommentApi* | [**videos_id_comment_threads_post**](docs/VideoCommentApi.md#videos_id_comment_threads_post) | **POST** /videos/{id}/comment-threads | Creates a comment thread, on a video by its id
*VideoCommentApi* | [**videos_id_comment_threads_thread_id_get**](docs/VideoCommentApi.md#videos_id_comment_threads_thread_id_get) | **GET** /videos/{id}/comment-threads/{threadId} | Get the comment thread by its id, of a video by its id
*VideoCommentApi* | [**videos_id_comments_comment_id_delete**](docs/VideoCommentApi.md#videos_id_comments_comment_id_delete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment in a comment thread by its id, of a video by its id
*VideoCommentApi* | [**videos_id_comments_comment_id_post**](docs/VideoCommentApi.md#videos_id_comments_comment_id_post) | **POST** /videos/{id}/comments/{commentId} | Creates a comment in a comment thread by its id, of a video by its id
*VideoPlaylistApi* | [**video_playlists_get**](docs/VideoPlaylistApi.md#video_playlists_get) | **GET** /video-playlists | Get list of video playlists
*VideoRateApi* | [**videos_id_rate_put**](docs/VideoRateApi.md#videos_id_rate_put) | **PUT** /videos/{id}/rate | Vote for a video by its id


## Documentation For Models

 - [Account](docs/Account.md)
 - [Actor](docs/Actor.md)
 - [AddUser](docs/AddUser.md)
 - [AddUserResponse](docs/AddUserResponse.md)
 - [Avatar](docs/Avatar.md)
 - [CommentThreadPostResponse](docs/CommentThreadPostResponse.md)
 - [CommentThreadResponse](docs/CommentThreadResponse.md)
 - [Follow](docs/Follow.md)
 - [GetMeVideoRating](docs/GetMeVideoRating.md)
 - [InlineObject](docs/InlineObject.md)
 - [InlineObject1](docs/InlineObject1.md)
 - [InlineObject2](docs/InlineObject2.md)
 - [InlineObject3](docs/InlineObject3.md)
 - [InlineObject4](docs/InlineObject4.md)
 - [InlineObject5](docs/InlineObject5.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [Job](docs/Job.md)
 - [PlaylistElement](docs/PlaylistElement.md)
 - [RegisterUser](docs/RegisterUser.md)
 - [RegisterUserChannel](docs/RegisterUserChannel.md)
 - [ServerConfig](docs/ServerConfig.md)
 - [ServerConfigAbout](docs/ServerConfigAbout.md)
 - [ServerConfigAboutInstance](docs/ServerConfigAboutInstance.md)
 - [ServerConfigAutoBlacklist](docs/ServerConfigAutoBlacklist.md)
 - [ServerConfigAutoBlacklistVideos](docs/ServerConfigAutoBlacklistVideos.md)
 - [ServerConfigAvatar](docs/ServerConfigAvatar.md)
 - [ServerConfigAvatarFile](docs/ServerConfigAvatarFile.md)
 - [ServerConfigAvatarFileSize](docs/ServerConfigAvatarFileSize.md)
 - [ServerConfigCustom](docs/ServerConfigCustom.md)
 - [ServerConfigCustomAdmin](docs/ServerConfigCustomAdmin.md)
 - [ServerConfigCustomCache](docs/ServerConfigCustomCache.md)
 - [ServerConfigCustomCachePreviews](docs/ServerConfigCustomCachePreviews.md)
 - [ServerConfigCustomFollowers](docs/ServerConfigCustomFollowers.md)
 - [ServerConfigCustomFollowersInstance](docs/ServerConfigCustomFollowersInstance.md)
 - [ServerConfigCustomInstance](docs/ServerConfigCustomInstance.md)
 - [ServerConfigCustomServices](docs/ServerConfigCustomServices.md)
 - [ServerConfigCustomServicesTwitter](docs/ServerConfigCustomServicesTwitter.md)
 - [ServerConfigCustomSignup](docs/ServerConfigCustomSignup.md)
 - [ServerConfigCustomTheme](docs/ServerConfigCustomTheme.md)
 - [ServerConfigCustomTranscoding](docs/ServerConfigCustomTranscoding.md)
 - [ServerConfigCustomTranscodingResolutions](docs/ServerConfigCustomTranscodingResolutions.md)
 - [ServerConfigEmail](docs/ServerConfigEmail.md)
 - [ServerConfigImport](docs/ServerConfigImport.md)
 - [ServerConfigImportVideos](docs/ServerConfigImportVideos.md)
 - [ServerConfigInstance](docs/ServerConfigInstance.md)
 - [ServerConfigInstanceCustomizations](docs/ServerConfigInstanceCustomizations.md)
 - [ServerConfigPlugin](docs/ServerConfigPlugin.md)
 - [ServerConfigSignup](docs/ServerConfigSignup.md)
 - [ServerConfigTranscoding](docs/ServerConfigTranscoding.md)
 - [ServerConfigTrending](docs/ServerConfigTrending.md)
 - [ServerConfigTrendingVideos](docs/ServerConfigTrendingVideos.md)
 - [ServerConfigUser](docs/ServerConfigUser.md)
 - [ServerConfigVideo](docs/ServerConfigVideo.md)
 - [ServerConfigVideoCaption](docs/ServerConfigVideoCaption.md)
 - [ServerConfigVideoCaptionFile](docs/ServerConfigVideoCaptionFile.md)
 - [ServerConfigVideoFile](docs/ServerConfigVideoFile.md)
 - [ServerConfigVideoImage](docs/ServerConfigVideoImage.md)
 - [UpdateMe](docs/UpdateMe.md)
 - [UpdateUser](docs/UpdateUser.md)
 - [User](docs/User.md)
 - [UserWatchingVideo](docs/UserWatchingVideo.md)
 - [Video](docs/Video.md)
 - [VideoAbuse](docs/VideoAbuse.md)
 - [VideoAbuseVideo](docs/VideoAbuseVideo.md)
 - [VideoAccountSummary](docs/VideoAccountSummary.md)
 - [VideoBlacklist](docs/VideoBlacklist.md)
 - [VideoCaption](docs/VideoCaption.md)
 - [VideoChannel](docs/VideoChannel.md)
 - [VideoChannelCreate](docs/VideoChannelCreate.md)
 - [VideoChannelOwnerAccount](docs/VideoChannelOwnerAccount.md)
 - [VideoChannelSummary](docs/VideoChannelSummary.md)
 - [VideoChannelUpdate](docs/VideoChannelUpdate.md)
 - [VideoComment](docs/VideoComment.md)
 - [VideoCommentThreadTree](docs/VideoCommentThreadTree.md)
 - [VideoConstantNumber](docs/VideoConstantNumber.md)
 - [VideoConstantString](docs/VideoConstantString.md)
 - [VideoDetails](docs/VideoDetails.md)
 - [VideoDetailsAllOf](docs/VideoDetailsAllOf.md)
 - [VideoFile](docs/VideoFile.md)
 - [VideoImport](docs/VideoImport.md)
 - [VideoImportState](docs/VideoImportState.md)
 - [VideoImportStateConstant](docs/VideoImportStateConstant.md)
 - [VideoListResponse](docs/VideoListResponse.md)
 - [VideoPlaylist](docs/VideoPlaylist.md)
 - [VideoPlaylistOwnerAccount](docs/VideoPlaylistOwnerAccount.md)
 - [VideoPlaylistPrivacy](docs/VideoPlaylistPrivacy.md)
 - [VideoPrivacyConstant](docs/VideoPrivacyConstant.md)
 - [VideoPrivacySet](docs/VideoPrivacySet.md)
 - [VideoRating](docs/VideoRating.md)
 - [VideoResolutionConstant](docs/VideoResolutionConstant.md)
 - [VideoScheduledUpdate](docs/VideoScheduledUpdate.md)
 - [VideoStateConstant](docs/VideoStateConstant.md)
 - [VideoStreamingPlaylists](docs/VideoStreamingPlaylists.md)
 - [VideoStreamingPlaylistsRedundancies](docs/VideoStreamingPlaylistsRedundancies.md)
 - [VideoUploadResponse](docs/VideoUploadResponse.md)
 - [VideoUserHistory](docs/VideoUserHistory.md)


## Documentation For Authorization


## OAuth2

- **Type**: OAuth
- **Flow**: password
- **Authorization URL**: 
- **Scopes**: 
 - **admin**: Admin scope
 - **moderator**: Moderator scope
 - **user**: User scope


## Author





## License

Copyright (C) 2015-2020 PeerTube Contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see http://www.gnu.org/licenses.

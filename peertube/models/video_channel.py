# coding: utf-8

"""
    PeerTube

    # Introduction  The PeerTube API is built on HTTP(S). Our API is RESTful. It has predictable resource URLs. It returns HTTP response codes to indicate errors. It also accepts and returns JSON in the HTTP body. You can use your favorite HTTP/REST library for your programming language to use PeerTube. No official SDK is currently provided, but the spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ```   # noqa: E501

    The version of the OpenAPI document: 2.1.0
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from peertube.configuration import Configuration


class VideoChannel(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'display_name': 'str',
        'description': 'str',
        'is_local': 'bool',
        'owner_account': 'VideoChannelOwnerAccount'
    }

    attribute_map = {
        'display_name': 'displayName',
        'description': 'description',
        'is_local': 'isLocal',
        'owner_account': 'ownerAccount'
    }

    def __init__(self, display_name=None, description=None, is_local=None, owner_account=None, local_vars_configuration=None):  # noqa: E501
        """VideoChannel - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._display_name = None
        self._description = None
        self._is_local = None
        self._owner_account = None
        self.discriminator = None

        if display_name is not None:
            self.display_name = display_name
        if description is not None:
            self.description = description
        if is_local is not None:
            self.is_local = is_local
        if owner_account is not None:
            self.owner_account = owner_account

    @property
    def display_name(self):
        """Gets the display_name of this VideoChannel.  # noqa: E501


        :return: The display_name of this VideoChannel.  # noqa: E501
        :rtype: str
        """
        return self._display_name

    @display_name.setter
    def display_name(self, display_name):
        """Sets the display_name of this VideoChannel.


        :param display_name: The display_name of this VideoChannel.  # noqa: E501
        :type: str
        """

        self._display_name = display_name

    @property
    def description(self):
        """Gets the description of this VideoChannel.  # noqa: E501


        :return: The description of this VideoChannel.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this VideoChannel.


        :param description: The description of this VideoChannel.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def is_local(self):
        """Gets the is_local of this VideoChannel.  # noqa: E501


        :return: The is_local of this VideoChannel.  # noqa: E501
        :rtype: bool
        """
        return self._is_local

    @is_local.setter
    def is_local(self, is_local):
        """Sets the is_local of this VideoChannel.


        :param is_local: The is_local of this VideoChannel.  # noqa: E501
        :type: bool
        """

        self._is_local = is_local

    @property
    def owner_account(self):
        """Gets the owner_account of this VideoChannel.  # noqa: E501


        :return: The owner_account of this VideoChannel.  # noqa: E501
        :rtype: VideoChannelOwnerAccount
        """
        return self._owner_account

    @owner_account.setter
    def owner_account(self, owner_account):
        """Sets the owner_account of this VideoChannel.


        :param owner_account: The owner_account of this VideoChannel.  # noqa: E501
        :type: VideoChannelOwnerAccount
        """

        self._owner_account = owner_account

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, VideoChannel):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, VideoChannel):
            return True

        return self.to_dict() != other.to_dict()

# peertube.VideoCommentApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_comment_threads_get**](VideoCommentApi.md#videos_id_comment_threads_get) | **GET** /videos/{id}/comment-threads | Get the comment threads of a video by its id
[**videos_id_comment_threads_post**](VideoCommentApi.md#videos_id_comment_threads_post) | **POST** /videos/{id}/comment-threads | Creates a comment thread, on a video by its id
[**videos_id_comment_threads_thread_id_get**](VideoCommentApi.md#videos_id_comment_threads_thread_id_get) | **GET** /videos/{id}/comment-threads/{threadId} | Get the comment thread by its id, of a video by its id
[**videos_id_comments_comment_id_delete**](VideoCommentApi.md#videos_id_comments_comment_id_delete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment in a comment thread by its id, of a video by its id
[**videos_id_comments_comment_id_post**](VideoCommentApi.md#videos_id_comments_comment_id_post) | **POST** /videos/{id}/comments/{commentId} | Creates a comment in a comment thread by its id, of a video by its id


# **videos_id_comment_threads_get**
> CommentThreadResponse videos_id_comment_threads_get(id, start=start, count=count, sort=sort)

Get the comment threads of a video by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCommentApi(api_client)
    id = 'id_example' # str | The video id or uuid
start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort comments by criteria (optional)

    try:
        # Get the comment threads of a video by its id
        api_response = api_instance.videos_id_comment_threads_get(id, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoCommentApi->videos_id_comment_threads_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort comments by criteria | [optional] 

### Return type

[**CommentThreadResponse**](CommentThreadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_comment_threads_post**
> CommentThreadPostResponse videos_id_comment_threads_post(id)

Creates a comment thread, on a video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCommentApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Creates a comment thread, on a video by its id
        api_response = api_instance.videos_id_comment_threads_post(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoCommentApi->videos_id_comment_threads_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_comment_threads_thread_id_get**
> VideoCommentThreadTree videos_id_comment_threads_thread_id_get(id, thread_id)

Get the comment thread by its id, of a video by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCommentApi(api_client)
    id = 'id_example' # str | The video id or uuid
thread_id = 3.4 # float | The thread id (root comment id)

    try:
        # Get the comment thread by its id, of a video by its id
        api_response = api_instance.videos_id_comment_threads_thread_id_get(id, thread_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoCommentApi->videos_id_comment_threads_thread_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **thread_id** | **float**| The thread id (root comment id) | 

### Return type

[**VideoCommentThreadTree**](VideoCommentThreadTree.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_comments_comment_id_delete**
> videos_id_comments_comment_id_delete(id, comment_id)

Delete a comment in a comment thread by its id, of a video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCommentApi(api_client)
    id = 'id_example' # str | The video id or uuid
comment_id = 3.4 # float | The comment id

    try:
        # Delete a comment in a comment thread by its id, of a video by its id
        api_instance.videos_id_comments_comment_id_delete(id, comment_id)
    except ApiException as e:
        print("Exception when calling VideoCommentApi->videos_id_comments_comment_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **comment_id** | **float**| The comment id | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_comments_comment_id_post**
> CommentThreadPostResponse videos_id_comments_comment_id_post(id, comment_id)

Creates a comment in a comment thread by its id, of a video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCommentApi(api_client)
    id = 'id_example' # str | The video id or uuid
comment_id = 3.4 # float | The comment id

    try:
        # Creates a comment in a comment thread by its id, of a video by its id
        api_response = api_instance.videos_id_comments_comment_id_post(id, comment_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoCommentApi->videos_id_comments_comment_id_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **comment_id** | **float**| The comment id | 

### Return type

[**CommentThreadPostResponse**](CommentThreadPostResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


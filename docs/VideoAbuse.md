# VideoAbuse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**reason** | **str** |  | [optional] 
**reporter_account** | [**Account**](Account.md) |  | [optional] 
**video** | [**VideoAbuseVideo**](VideoAbuseVideo.md) |  | [optional] 
**created_at** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



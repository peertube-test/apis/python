# VideoImport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**target_url** | **str** |  | [optional] 
**magnet_uri** | **str** |  | [optional] 
**torrent_name** | **str** |  | [optional] 
**state** | [**VideoImportState**](VideoImportState.md) |  | [optional] 
**error** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**video** | [**Video**](Video.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



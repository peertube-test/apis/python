# VideoStreamingPlaylists

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**type** | **float** | Playlist type (HLS &#x3D; 1) | [optional] 
**playlist_url** | **str** |  | [optional] 
**segments_sha256_url** | **str** |  | [optional] 
**redundancies** | [**list[VideoStreamingPlaylistsRedundancies]**](VideoStreamingPlaylistsRedundancies.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



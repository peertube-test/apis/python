# VideoCommentThreadTree

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | [**VideoComment**](VideoComment.md) |  | [optional] 
**children** | [**list[VideoCommentThreadTree]**](VideoCommentThreadTree.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



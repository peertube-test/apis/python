# Actor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**url** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**host** | **str** |  | [optional] 
**following_count** | **float** |  | [optional] 
**followers_count** | **float** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**avatar** | [**Avatar**](Avatar.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



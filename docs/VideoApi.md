# peertube.VideoApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_name_videos_get**](VideoApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | Get videos for an account, provided the name of that account
[**video_channels_channel_handle_videos_get**](VideoApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | Get videos of a video channel by its id
[**videos_categories_get**](VideoApi.md#videos_categories_get) | **GET** /videos/categories | Get list of video categories known by the server
[**videos_get**](VideoApi.md#videos_get) | **GET** /videos | Get list of videos
[**videos_id_delete**](VideoApi.md#videos_id_delete) | **DELETE** /videos/{id} | Delete a video by its id
[**videos_id_description_get**](VideoApi.md#videos_id_description_get) | **GET** /videos/{id}/description | Get a video description by its id
[**videos_id_get**](VideoApi.md#videos_id_get) | **GET** /videos/{id} | Get a video by its id
[**videos_id_give_ownership_post**](VideoApi.md#videos_id_give_ownership_post) | **POST** /videos/{id}/give-ownership | Request change of ownership for a video you own, by its id
[**videos_id_put**](VideoApi.md#videos_id_put) | **PUT** /videos/{id} | Update metadata for a video by its id
[**videos_id_views_post**](VideoApi.md#videos_id_views_post) | **POST** /videos/{id}/views | Add a view to the video by its id
[**videos_id_watching_put**](VideoApi.md#videos_id_watching_put) | **PUT** /videos/{id}/watching | Set watching progress of a video by its id for a user
[**videos_imports_post**](VideoApi.md#videos_imports_post) | **POST** /videos/imports | Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
[**videos_languages_get**](VideoApi.md#videos_languages_get) | **GET** /videos/languages | Get list of languages known by the server
[**videos_licences_get**](VideoApi.md#videos_licences_get) | **GET** /videos/licences | Get list of video licences known by the server
[**videos_ownership_get**](VideoApi.md#videos_ownership_get) | **GET** /videos/ownership | Get list of video ownership changes requests
[**videos_ownership_id_accept_post**](VideoApi.md#videos_ownership_id_accept_post) | **POST** /videos/ownership/{id}/accept | Refuse ownership change request for video by its id
[**videos_ownership_id_refuse_post**](VideoApi.md#videos_ownership_id_refuse_post) | **POST** /videos/ownership/{id}/refuse | Accept ownership change request for video by its id
[**videos_privacies_get**](VideoApi.md#videos_privacies_get) | **GET** /videos/privacies | Get list of privacy policies supported by the server
[**videos_upload_post**](VideoApi.md#videos_upload_post) | **POST** /videos/upload | Upload a video file with its metadata


# **accounts_name_videos_get**
> VideoListResponse accounts_name_videos_get(name)

Get videos for an account, provided the name of that account

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    name = 'name_example' # str | The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example)

    try:
        # Get videos for an account, provided the name of that account
        api_response = api_instance.accounts_name_videos_get(name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->accounts_name_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account (chocobozzz or chocobozzz@peertube.cpy.re for example) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_channel_handle_videos_get**
> VideoListResponse video_channels_channel_handle_videos_get(channel_handle)

Get videos of a video channel by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    channel_handle = 'channel_handle_example' # str | The video channel handle (example: 'my_username@example.com' or 'my_username')

    try:
        # Get videos of a video channel by its id
        api_response = api_instance.video_channels_channel_handle_videos_get(channel_handle)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->video_channels_channel_handle_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **str**| The video channel handle (example: &#39;my_username@example.com&#39; or &#39;my_username&#39;) | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_categories_get**
> list[str] videos_categories_get()

Get list of video categories known by the server

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # Get list of video categories known by the server
        api_response = api_instance.videos_categories_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_categories_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_get**
> VideoListResponse videos_get(category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, start=start, count=count, sort=sort)

Get list of videos

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    category_one_of = peertube.OneOfnumberarray() # OneOfnumberarray | category id of the video (optional)
tags_one_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video (optional)
tags_all_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
licence_one_of = peertube.OneOfnumberarray() # OneOfnumberarray | licence id of the video (optional)
language_one_of = peertube.OneOfstringarray() # OneOfstringarray | language id of the video (optional)
nsfw = 'nsfw_example' # str | whether to include nsfw videos, if any (optional)
filter = 'filter_example' # str | Special filters (local for instance) which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  (optional)
start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort videos by criteria (optional)

    try:
        # Get list of videos
        api_response = api_instance.videos_get(category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_one_of** | [**OneOfnumberarray**](.md)| category id of the video | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfnumberarray**](.md)| licence id of the video | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video | [optional] 
 **nsfw** | **str**| whether to include nsfw videos, if any | [optional] 
 **filter** | **str**| Special filters (local for instance) which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_delete**
> videos_id_delete(id)

Delete a video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Delete a video by its id
        api_instance.videos_id_delete(id)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_description_get**
> str videos_id_description_get(id)

Get a video description by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Get a video description by its id
        api_response = api_instance.videos_id_description_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_description_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_get**
> VideoDetails videos_id_get(id)

Get a video by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Get a video by its id
        api_response = api_instance.videos_id_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

[**VideoDetails**](VideoDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_give_ownership_post**
> videos_id_give_ownership_post(id, username)

Request change of ownership for a video you own, by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid
username = 'username_example' # str | 

    try:
        # Request change of ownership for a video you own, by its id
        api_instance.videos_id_give_ownership_post(id, username)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_give_ownership_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **username** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |
**400** | Changing video ownership to a remote account is not supported yet |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_put**
> videos_id_put(id, thumbnailfile=thumbnailfile, previewfile=previewfile, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, name=name, tags=tags, comments_enabled=comments_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)

Update metadata for a video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid
thumbnailfile = '/path/to/file' # file | Video thumbnail file (optional)
previewfile = '/path/to/file' # file | Video preview file (optional)
category = 'category_example' # str | Video category (optional)
licence = 'licence_example' # str | Video licence (optional)
language = 'language_example' # str | Video language (optional)
description = 'description_example' # str | Video description (optional)
wait_transcoding = 'wait_transcoding_example' # str | Whether or not we wait transcoding before publish the video (optional)
support = 'support_example' # str | Text describing how to support the video uploader (optional)
nsfw = 'nsfw_example' # str | Whether or not this video contains sensitive content (optional)
name = 'name_example' # str | Video name (optional)
tags = 'tags_example' # list[str] | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = 'comments_enabled_example' # str | Enable or disable comments for this video (optional)
originally_published_at = '2013-10-20T19:20:30+01:00' # datetime | Date when the content was originally published (optional)
schedule_update = peertube.VideoScheduledUpdate() # VideoScheduledUpdate |  (optional)

    try:
        # Update metadata for a video by its id
        api_instance.videos_id_put(id, thumbnailfile=thumbnailfile, previewfile=previewfile, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, name=name, tags=tags, comments_enabled=comments_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **thumbnailfile** | **file**| Video thumbnail file | [optional] 
 **previewfile** | **file**| Video preview file | [optional] 
 **category** | **str**| Video category | [optional] 
 **licence** | **str**| Video licence | [optional] 
 **language** | **str**| Video language | [optional] 
 **description** | **str**| Video description | [optional] 
 **wait_transcoding** | **str**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **str**| Text describing how to support the video uploader | [optional] 
 **nsfw** | **str**| Whether or not this video contains sensitive content | [optional] 
 **name** | **str**| Video name | [optional] 
 **tags** | [**list[str]**](str.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **str**| Enable or disable comments for this video | [optional] 
 **originally_published_at** | **datetime**| Date when the content was originally published | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_views_post**
> videos_id_views_post(id)

Add a view to the video by its id

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Add a view to the video by its id
        api_instance.videos_id_views_post(id)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_views_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_watching_put**
> videos_id_watching_put(id, user_watching_video)

Set watching progress of a video by its id for a user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid
user_watching_video = peertube.UserWatchingVideo() # UserWatchingVideo | 

    try:
        # Set watching progress of a video by its id for a user
        api_instance.videos_id_watching_put(id, user_watching_video)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_watching_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 
 **user_watching_video** | [**UserWatchingVideo**](UserWatchingVideo.md)|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_imports_post**
> VideoUploadResponse videos_imports_post(channel_id, name, torrentfile=torrentfile, target_url=target_url, magnet_uri=magnet_uri, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, schedule_update=schedule_update)

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    channel_id = 3.4 # float | Channel id that will contain this video
name = 'name_example' # str | Video name
torrentfile = '/path/to/file' # file | Torrent File (optional)
target_url = 'target_url_example' # str | HTTP target URL (optional)
magnet_uri = 'magnet_uri_example' # str | Magnet URI (optional)
thumbnailfile = '/path/to/file' # file | Video thumbnail file (optional)
previewfile = '/path/to/file' # file | Video preview file (optional)
privacy = peertube.VideoPrivacySet() # VideoPrivacySet |  (optional)
category = 'category_example' # str | Video category (optional)
licence = 'licence_example' # str | Video licence (optional)
language = 'language_example' # str | Video language (optional)
description = 'description_example' # str | Video description (optional)
wait_transcoding = 'wait_transcoding_example' # str | Whether or not we wait transcoding before publish the video (optional)
support = 'support_example' # str | Text describing how to support the video uploader (optional)
nsfw = 'nsfw_example' # str | Whether or not this video contains sensitive content (optional)
tags = 'tags_example' # list[str] | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = 'comments_enabled_example' # str | Enable or disable comments for this video (optional)
schedule_update = peertube.VideoScheduledUpdate() # VideoScheduledUpdate |  (optional)

    try:
        # Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)
        api_response = api_instance.videos_imports_post(channel_id, name, torrentfile=torrentfile, target_url=target_url, magnet_uri=magnet_uri, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, schedule_update=schedule_update)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_imports_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **float**| Channel id that will contain this video | 
 **name** | **str**| Video name | 
 **torrentfile** | **file**| Torrent File | [optional] 
 **target_url** | **str**| HTTP target URL | [optional] 
 **magnet_uri** | **str**| Magnet URI | [optional] 
 **thumbnailfile** | **file**| Video thumbnail file | [optional] 
 **previewfile** | **file**| Video preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **str**| Video category | [optional] 
 **licence** | **str**| Video licence | [optional] 
 **language** | **str**| Video language | [optional] 
 **description** | **str**| Video description | [optional] 
 **wait_transcoding** | **str**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **str**| Text describing how to support the video uploader | [optional] 
 **nsfw** | **str**| Whether or not this video contains sensitive content | [optional] 
 **tags** | [**list[str]**](str.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **str**| Enable or disable comments for this video | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_languages_get**
> list[str] videos_languages_get()

Get list of languages known by the server

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # Get list of languages known by the server
        api_response = api_instance.videos_languages_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_languages_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_licences_get**
> list[str] videos_licences_get()

Get list of video licences known by the server

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # Get list of video licences known by the server
        api_response = api_instance.videos_licences_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_licences_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_ownership_get**
> videos_ownership_get()

Get list of video ownership changes requests

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # Get list of video ownership changes requests
        api_instance.videos_ownership_get()
    except ApiException as e:
        print("Exception when calling VideoApi->videos_ownership_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_ownership_id_accept_post**
> videos_ownership_id_accept_post(id)

Refuse ownership change request for video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Refuse ownership change request for video by its id
        api_instance.videos_ownership_id_accept_post(id)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_ownership_id_accept_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_ownership_id_refuse_post**
> videos_ownership_id_refuse_post(id)

Accept ownership change request for video by its id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = 'id_example' # str | The video id or uuid

    try:
        # Accept ownership change request for video by its id
        api_instance.videos_ownership_id_refuse_post(id)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_ownership_id_refuse_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The video id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** |  |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_privacies_get**
> list[str] videos_privacies_get()

Get list of privacy policies supported by the server

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # Get list of privacy policies supported by the server
        api_response = api_instance.videos_privacies_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_privacies_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_upload_post**
> VideoUploadResponse videos_upload_post(videofile, channel_id, name, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)

Upload a video file with its metadata

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
configuration = peertube.Configuration()
# Configure OAuth2 access token for authorization: OAuth2
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Defining host is optional and default to https://peertube.cpy.re/api/v1
configuration.host = "https://peertube.cpy.re/api/v1"
# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    videofile = '/path/to/file' # file | Video file
channel_id = 3.4 # float | Channel id that will contain this video
name = 'name_example' # str | Video name
thumbnailfile = '/path/to/file' # file | Video thumbnail file (optional)
previewfile = '/path/to/file' # file | Video preview file (optional)
privacy = peertube.VideoPrivacySet() # VideoPrivacySet |  (optional)
category = 'category_example' # str | Video category (optional)
licence = 'licence_example' # str | Video licence (optional)
language = 'language_example' # str | Video language (optional)
description = 'description_example' # str | Video description (optional)
wait_transcoding = 'wait_transcoding_example' # str | Whether or not we wait transcoding before publish the video (optional)
support = 'support_example' # str | Text describing how to support the video uploader (optional)
nsfw = 'nsfw_example' # str | Whether or not this video contains sensitive content (optional)
tags = 'tags_example' # list[str] | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = 'comments_enabled_example' # str | Enable or disable comments for this video (optional)
originally_published_at = '2013-10-20T19:20:30+01:00' # datetime | Date when the content was originally published (optional)
schedule_update = peertube.VideoScheduledUpdate() # VideoScheduledUpdate |  (optional)

    try:
        # Upload a video file with its metadata
        api_response = api_instance.videos_upload_post(videofile, channel_id, name, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_upload_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videofile** | **file**| Video file | 
 **channel_id** | **float**| Channel id that will contain this video | 
 **name** | **str**| Video name | 
 **thumbnailfile** | **file**| Video thumbnail file | [optional] 
 **previewfile** | **file**| Video preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **str**| Video category | [optional] 
 **licence** | **str**| Video licence | [optional] 
 **language** | **str**| Video language | [optional] 
 **description** | **str**| Video description | [optional] 
 **wait_transcoding** | **str**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **str**| Text describing how to support the video uploader | [optional] 
 **nsfw** | **str**| Whether or not this video contains sensitive content | [optional] 
 **tags** | [**list[str]**](str.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **str**| Enable or disable comments for this video | [optional] 
 **originally_published_at** | **datetime**| Date when the content was originally published | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**403** | The user video quota is exceeded with this video. |  -  |
**408** | Upload has timed out |  -  |
**422** | Invalid input file. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# VideoPlaylist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 
**display_name** | **str** |  | [optional] 
**is_local** | **bool** |  | [optional] 
**video_length** | **float** |  | [optional] 
**thumbnail_path** | **str** |  | [optional] 
**privacy** | [**VideoPlaylistPrivacy**](VideoPlaylistPrivacy.md) |  | [optional] 
**type** | [**VideoPlaylistPrivacy**](VideoPlaylistPrivacy.md) |  | [optional] 
**owner_account** | [**VideoPlaylistOwnerAccount**](VideoPlaylistOwnerAccount.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# peertube.SearchApi

All URIs are relative to *https://peertube.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search_videos_get**](SearchApi.md#search_videos_get) | **GET** /search/videos | Get the videos corresponding to a given query


# **search_videos_get**
> VideoListResponse search_videos_get(search, start=start, count=count, sort=sort)

Get the videos corresponding to a given query

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.SearchApi(api_client)
    search = 'search_example' # str | String to search
start = 3.4 # float | Offset (optional)
count = 3.4 # float | Number of items (optional)
sort = 'sort_example' # str | Sort videos by criteria (optional)

    try:
        # Get the videos corresponding to a given query
        api_response = api_instance.search_videos_get(search, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling SearchApi->search_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **str**| String to search | 
 **start** | **float**| Offset | [optional] 
 **count** | **float**| Number of items | [optional] 
 **sort** | **str**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


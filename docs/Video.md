# Video

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** |  | [optional] 
**uuid** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**published_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**originally_published_at** | **str** |  | [optional] 
**category** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**licence** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**language** | [**VideoConstantString**](VideoConstantString.md) |  | [optional] 
**privacy** | [**VideoPrivacyConstant**](VideoPrivacyConstant.md) |  | [optional] 
**description** | **str** |  | [optional] 
**duration** | **float** |  | [optional] 
**is_local** | **bool** |  | [optional] 
**name** | **str** |  | [optional] 
**thumbnail_path** | **str** |  | [optional] 
**preview_path** | **str** |  | [optional] 
**embed_path** | **str** |  | [optional] 
**views** | **float** |  | [optional] 
**likes** | **float** |  | [optional] 
**dislikes** | **float** |  | [optional] 
**nsfw** | **bool** |  | [optional] 
**wait_transcoding** | **bool** |  | [optional] 
**state** | [**VideoStateConstant**](VideoStateConstant.md) |  | [optional] 
**scheduled_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 
**blacklisted** | **bool** |  | [optional] 
**blacklisted_reason** | **str** |  | [optional] 
**account** | [**VideoAccountSummary**](VideoAccountSummary.md) |  | [optional] 
**channel** | [**VideoChannelSummary**](VideoChannelSummary.md) |  | [optional] 
**user_history** | [**VideoUserHistory**](VideoUserHistory.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



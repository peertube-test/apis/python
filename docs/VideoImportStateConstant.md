# VideoImportStateConstant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The video import state (Pending &#x3D; 1, Success &#x3D; 2, Failed &#x3D; 3) | [optional] 
**label** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



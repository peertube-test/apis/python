# ServerConfigCustomTranscodingResolutions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_240p** | **bool** |  | [optional] 
**_360p** | **bool** |  | [optional] 
**_480p** | **bool** |  | [optional] 
**_720p** | **bool** |  | [optional] 
**_1080p** | **bool** |  | [optional] 
**_2160p** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


